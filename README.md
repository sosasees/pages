# poppy ribbon's website

made with [BEM](https://getbem.com)

## LICENSES

each file may have a different license —
the more precise license definitions will come when the website develops.
for now:
- all HTML pages are licensed under [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/).
- the CSS stylesheet is licensed under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/).
- my font [poppy ribbon desktop](https://codeberg.org/sosasees/poppy-ribbon-desktop-font) is licensed under [SIL OFL 1.1](https://openfontlicense.org/open-font-license-official-text/)
